package com.ryan.remindernotification;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;


public class ReminderaActivity extends Activity {
    private int PICK_IMAGE_REQUEST = 1;
    private int CROP_IMAGE_REQUEST = 2;
    public Bitmap TEMP_PHOTO_FILE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remindera);

        TEMP_PHOTO_FILE = BitmapFactory.decodeResource(getResources(), R.drawable.rex_png);

        Button wearButton = (Button)findViewById(R.id.wearButton);
        Button intentGalleryButton = (Button) findViewById(R.id.galleryButton);
        intentGalleryButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(galleryIntent, "Select Image"), PICK_IMAGE_REQUEST);


            }

        });

        wearButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int notificationId = 001;

                final String wearNotifText = ((EditText)findViewById(R.id.wearNotifTextField)).getText().toString();
                final String wearNotifTitle = ((EditText)findViewById(R.id.wearNotifTitleField)).getText().toString();
//                final Bitmap TEMP_PHOTO_FILE = BitmapFactory.decodeResource(getResources(), R.drawable.rex_png);
                final Bitmap notifIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(ReminderaActivity.this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(wearNotifTitle)
                            .setContentText(wearNotifText)
                            .setLargeIcon(notifIcon)
                            .extend(new NotificationCompat.WearableExtender().setBackground(TEMP_PHOTO_FILE));

                long[] pattern = {500, 500};
                notificationBuilder.setVibrate(pattern);

                NotificationManagerCompat notificationManager =
                        NotificationManagerCompat.from(ReminderaActivity.this);

                notificationManager.notify(notificationId, notificationBuilder.build());};

        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();


            try {
                Bitmap galleryBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                 Log.d("CUSTOM ERROR", String.valueOf(galleryBitmap));
                TEMP_PHOTO_FILE = galleryBitmap;
                Log.v("REACH_TAG", "NOTIFICATION BACKGROUND SET REACHED");

                ImageView imageView = (ImageView) findViewById(R.id.selectedGallery);
                imageView.setImageBitmap(galleryBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == CROP_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(uri, "image/*");
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 400);
            cropIntent.putExtra("outputY", 400);
            cropIntent.putExtra("scale", true);

            // retrieve data on return
//            cropIntent.putExtra("return-data", true);

            Log.v("REACH_TAG", "GOT TO CROP RESPONSE");
            startActivityForResult(cropIntent, PICK_IMAGE_REQUEST);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_remindera, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
