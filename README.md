# README #

### What is this repository for? ###

* Practice of my skills in Android and Java
* Such as: Android wearables, notification services, intents, bitmaps, layouts

### How do I get set up? ###

* Clone this repository
* Open the project in android studio
* Run the project on an emulated or real Android device on Android API Level 22 or higher